# Gitlab Ci Runner / Zephyr EC runner container

This image is based on the Ubuntu images and adds what is required for Zephyr EC
to be able to be built and run its test suite.

## Usage

- Install docker

See http://go/installdocker. Setup sudo-less docker.

Log into [docker hub](https://hub.docker.com/)
```
docker login
```

- Build this image on your docker host, e.g.:

```
user=$(docker info | awk '/Username:/ { print $2 }')
tag=$user/ubuntu-$(date +%d%b%y | tr 'A-Z' 'a-z')
docker build -t $tag .
```

- Test it locally (press Ctrl-D to exit)

docker run -it $tag

- Try pushing it, e.g.:

```
docker push $tag
```

- Update the image: tag in the zephyr-ec repo's .gitlab-ci.yml if needed, and
  push a new commit there.
