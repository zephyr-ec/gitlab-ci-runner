# SPDX-License-Identifier: GPL-2.0+
# This Dockerfile is used to build an image containing basic stuff to be used
# to build Zephyr EC and run our test suites.

FROM ubuntu:focal-20220113

LABEL org.opencontainers.image.authors="Simon Glass <sjg@chromium.org>"
LABEL Description=" This image is for building Zephyr EC inside a container"

# Make sure apt is happy
ENV DEBIAN_FRONTEND=noninteractive

# The directory containing the zephyr sources (main and cmsis)
ENV ZEPHYR_DIR=/zephyr

# Temporary directory for building things
ENV TMP_DIR=/tmp/build

# Add LLVM repository
RUN apt-get update && apt-get install -y gnupg2 wget xz-utils && rm -rf /var/lib/apt/lists/*
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
RUN echo deb http://apt.llvm.org/focal/ llvm-toolchain-focal-13 main | tee /etc/apt/sources.list.d/llvm.list

# Manually install the kernel.org "Crosstool" based toolchains for gcc-11.1.0
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-aarch64-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-arm-linux-gnueabi.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-i386-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-m68k-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-mips-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-microblaze-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-nios2-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-powerpc-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-riscv64-linux.tar.xz | tar -C /opt -xJ
RUN wget --progress=bar:force:noscroll -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/11.1.0/x86_64-gcc-11.1.0-nolibc-sh2-linux.tar.xz | tar -C /opt -xJ

# Manually install other toolchains
RUN wget --progress=bar:force:noscroll -O - https://github.com/foss-xtensa/toolchain/releases/download/2020.07/x86_64-2020.07-xtensa-dc233c-elf.tar.gz | tar -C /opt -xz
RUN wget --progress=bar:force:noscroll -O - https://github.com/foss-for-synopsys-dwc-arc-processors/toolchain/releases/download/arc-2021.03-release/arc_gnu_2021.03_prebuilt_uclibc_le_archs_linux_install.tar.gz | tar --no-same-owner -C /opt -xz
RUN wget --progress=bar:force:noscroll -O - https://github.com/vincentzwc/prebuilt-nds32-toolchain/releases/download/20180521/nds32le-linux-glibc-v3-upstream.tar.gz | tar -C /opt -xz

# Update and install things from apt
RUN apt-get update && apt-get install -y \
	apt-transport-https \
	automake \
	autopoint \
	bc \
	binutils-dev \
	bison \
	build-essential \
	ca-certificates \
	ccache \
	clang-13 \
	cmake \
	coreutils \
	cpio \
	cppcheck \
	curl \
	device-tree-compiler \
	dosfstools \
	e2fsprogs \
	efitools \
	expect \
	fakeroot \
	file \
	flex \
	g++-multilib \
	gawk \
	gcc-multilib \
	gdisk \
	git \
	gnu-efi \
	gnupg \
	gnutls-dev \
	golang-go \
	graphviz \
	grub-efi-amd64-bin \
	grub-efi-ia32-bin \
	help2man \
	iasl \
	imagemagick \
	iputils-ping \
	jq \
	libconfuse-dev \
	libfdt-dev \
	libftdi-dev \
	libgit2-dev \
	libjson-glib-dev \
	libgnutls28-dev \
	libgnutls30 \
	libguestfs-tools \
	liblz4-tool \
	libpixman-1-dev \
	libpython3-dev \
	libsdl1.2-dev \
	libsdl2-dev \
	libseccomp-dev \
	libssl-dev \
	libtool \
	libudev-dev \
	libusb-1.0 \
	libusb-1.0-0-dev \
	linux-image-kvm \
	lzma-alone \
	lzop \
	mount \
	mtd-utils \
	mtools \
	net-tools \
	ninja-build \
	openssl \
	picocom \
	parted \
	pkg-config \
	python-is-python3 \
	python2.7 \
	python3 \
	python3-dev \
	python3-pip \
	python3-pyelftools \
	python3-setuptools \
	python3-sphinx \
	python3-virtualenv \
	rpm2cpio \
	sbsigntool \
	sloccount \
	socat \
	softhsm2 \
	software-properties-common \
	sparse \
	srecord \
	sudo \
	swig \
	util-linux \
	uuid-dev \
	virtualenv \
	xxd \
	zip \
	&& rm -rf /var/lib/apt/lists/*

# Make kernels readable for libguestfs tools to work correctly
RUN chmod +r /boot/vmlinu*

# Things that Zephyr asks for but we don't need yet, so skip them to save space
# g++-multilib
# gcc-multilib
# gperf
# libsdl2-dev
# python3-tk
# python3-wheel

# Zephyr wants cmake >= 3.20.0, ubuntu has 3.16.3
# Get a key so we can add the cmake repository
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null

# Get a later version of cmake
RUN apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'

# Update cmake to at least the minimum required by Zephyr
RUN apt-get update && apt-get install -y \
	cmake \
	&& rm -rf /var/lib/apt/lists/*

# Install older libssl1 for futility
RUN apt-add-repository 'deb http://security.ubuntu.com/ubuntu/ bionic-security main'
RUN sudo apt-get update && apt-get install -y --no-install-recommends libssl1.0.0

# lcov dependedncies
RUN apt-get install -y \
	libcapture-tiny-perl libdatetime-perl libdevel-cover-perl libdigest-perl-md5-perl \
	libfile-spec-native-perl libjson-xs-perl \
	libmodule-load-conditional-perl libscalar-util-numeric-perl 
# We need lcov >= 2, ubuntu has 1.14	
# # Get a later version of lcov, switch to `apt-get install lcov` when version >= 2
RUN mkdir /tmp/lcov
RUN cd /tmp/lcov && \
	git clone https://github.com/linux-test-project/lcov.git && \
	cd lcov && \
	make install PREFIX='/usr' && \
	rm -rf /tmp/lcov

# Some other things Zephyr wants
RUN pip3 install \
	'pyelftools>=0.28' \
	anytree \
	colorama \
	natsort \
	packaging \
	ply \
	psutil \
	pykwalify \
	pyyaml

# Install the SDK but remove almost everything except for ARM to save space
# Get version number from https://github.com/zephyrproject-rtos/sdk-ng/releases/latest
ENV ver=0.16.1
ENV host=linux-x86_64
RUN wget --progress=bar:force:noscroll https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${ver}/zephyr-sdk-${ver}_${host}.tar.xz && \
	tar xvf zephyr-sdk-${ver}_${host}.tar.xz && \
	mv zephyr-sdk-${ver} /opt/zephyr-sdk && \
	rm -rf /opt/zephyr-sdk/aarch64-zephyr-elf/ \
	/opt/zephyr-sdk/arc-zephyr-elf/ \
	/opt/zephyr-sdk/nios2-zephyr-elf/ \
	/opt/zephyr-sdk/sparc-zephyr-elf/ \
	/opt/zephyr-sdk/x86_64-zephyr-elf/ \
	/opt/zephyr-sdk/xtensa/ \
	zephyr-sdk-${ver}_${host}.tar.xz && \
	( cd /opt/zephyr-sdk && ./setup.sh -t all -h )

# Zephyr wants dtc >= 1.4.6, ubuntu has 1.5.0, however ubuntu doesn't have pylibfdt at all
# Install the latest dtc and pylibfdt
# Copy them into dist-packages since site-packages is not on the PYTHONPATH
RUN mkdir -p $TMP_DIR/dtc && \
	git clone --depth=1 --branch v1.6.0 --single-branch \
	https://git.kernel.org/pub/scm/utils/dtc/dtc.git \
	$TMP_DIR/dtc && \
	make -C $TMP_DIR/dtc install PREFIX=/usr && \
	cp -r /usr/lib/python3.8/site-packages/* /usr/lib/python3/dist-packages/ && \
	rm -rf $TMP_DIR/dtc

# Install binman and the patman libraries it needs
RUN mkdir -p $TMP_DIR/u-boot && \
	git clone --depth=1 --branch chromeos-v2023.04 --single-branch \
	https://chromium.googlesource.com/chromiumos/third_party/u-boot \
	$TMP_DIR/u-boot && \
	cd $TMP_DIR/u-boot/tools/patman && \
	python3 setup.py install --prefix=/usr --install-layout=deb && \
	cd $TMP_DIR/u-boot/tools/dtoc && \
	python3 setup.py install --prefix=/usr --install-layout=deb && \
	cd $TMP_DIR/u-boot/tools/binman && \
	python3 setup.py install --prefix=/usr --install-layout=deb && \
	rm -rf $TMP_DIR/u-boot

# We could copy in Zephyr's dtc but we are building it below, so don't.
# RUN cp /opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/bin/dtc /usr/bin

# Now various U-Boot things from https://github.com/u-boot/u-boot/tree/master/tools/docker
# Manually install libmpfr4 for the toolchains
RUN wget http://mirrors.kernel.org/ubuntu/pool/main/m/mpfr4/libmpfr4_3.1.4-1_amd64.deb && dpkg -i libmpfr4_3.1.4-1_amd64.deb && rm libmpfr4_3.1.4-1_amd64.deb

# Manually install a new enough version of efitools (must be v1.5.2 or later)
RUN wget http://mirrors.kernel.org/ubuntu/pool/universe/e/efitools/efitools_1.8.1-0ubuntu2_amd64.deb && sudo dpkg -i efitools_1.8.1-0ubuntu2_amd64.deb && rm efitools_1.8.1-0ubuntu2_amd64.deb

# Manually install a new enough version of sbsigntools (must be v0.9.4 or later)
RUN git clone https://git.kernel.org/pub/scm/linux/kernel/git/jejb/sbsigntools.git /tmp/sbsigntools && \
	cd /tmp/sbsigntools && \
	git checkout -b latest v0.9.4 && \
	./autogen.sh && \
	./configure && \
	make && \
	make install && \
	rm -rf /tmp/sbsigntools

# Build GRUB UEFI targets for ARM & RISC-V, 32-bit and 64-bit
RUN git clone git://git.savannah.gnu.org/grub.git /tmp/grub && \
	cd /tmp/grub && \
	git checkout grub-2.06 && \
	./bootstrap && \
	mkdir -p /opt/grub && \
	./configure --target=aarch64 --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-11.1.0-nolibc/aarch64-linux/bin/aarch64-linux-gcc \
	TARGET_OBJCOPY=/opt/gcc-11.1.0-nolibc/aarch64-linux/bin/aarch64-linux-objcopy \
	TARGET_STRIP=/opt/gcc-11.1.0-nolibc/aarch64-linux/bin/aarch64-linux-strip \
	TARGET_NM=/opt/gcc-11.1.0-nolibc/aarch64-linux/bin/aarch64-linux-nm \
	TARGET_RANLIB=/opt/gcc-11.1.0-nolibc/aarch64-linux/bin/aarch64-linux-ranlib && \
	make && \
	./grub-mkimage -O arm64-efi -o /opt/grub/grubaa64.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	make clean && \
	./configure --target=arm --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-11.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-gcc \
	TARGET_OBJCOPY=/opt/gcc-11.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-objcopy \
	TARGET_STRIP=/opt/gcc-11.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-strip \
	TARGET_NM=/opt/gcc-11.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-nm \
	TARGET_RANLIB=/opt/gcc-11.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-ranlib && \
	make && \
	./grub-mkimage -O arm-efi -o /opt/grub/grubarm.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	make clean && \
	./configure --target=riscv64 --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-11.1.0-nolibc/riscv64-linux/bin/riscv64-linux-gcc \
	TARGET_OBJCOPY=/opt/gcc-11.1.0-nolibc/riscv64-linux/bin/riscv64-linux-objcopy \
	TARGET_STRIP=/opt/gcc-11.1.0-nolibc/riscv64-linux/bin/riscv64-linux-strip \
	TARGET_NM=/opt/gcc-11.1.0-nolibc/riscv64-linux/bin/riscv64-linux-nm \
	TARGET_RANLIB=/opt/gcc-11.1.0-nolibc/riscv64-linux/bin/riscv64-linux-ranlib && \
	make && \
	./grub-mkimage -O riscv64-efi -o /opt/grub/grubriscv64.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	rm -rf /tmp/grub

RUN git clone https://gitlab.com/qemu-project/qemu.git /tmp/qemu && \
	cd /tmp/qemu && \
	git checkout v6.1.0 && \
	git submodule update --init dtc && \
	# config user.name and user.email to make 'git am' happy
	git config user.name u-boot && \
	git config user.email u-boot@denx.de && \
	# manually apply the bug fix for QEMU 6.1.0 Xilinx Zynq UART emulation codes
	wget -O - http://patchwork.ozlabs.org/project/qemu-devel/patch/20210823020813.25192-2-bmeng.cn@gmail.com/mbox/ | git am && \
	./configure --prefix=/opt/qemu --target-list="aarch64-softmmu,arm-softmmu,i386-softmmu,mips-softmmu,mips64-softmmu,mips64el-softmmu,mipsel-softmmu,ppc-softmmu,riscv32-softmmu,riscv64-softmmu,sh4-softmmu,x86_64-softmmu,xtensa-softmmu" && \
	make -j$(nproc) all install && \
	rm -rf /tmp/qemu

# Build genimage (required by some targets to generate disk images)
RUN wget -O - https://github.com/pengutronix/genimage/releases/download/v14/genimage-14.tar.xz | tar -C /tmp -xJ && \
	cd /tmp/genimage-14 && \
	./configure && \
	make -j$(nproc) && \
	make install && \
	rm -rf /tmp/genimage-14

# Build libtpms
RUN git clone https://github.com/stefanberger/libtpms /tmp/libtpms && \
	cd /tmp/libtpms && \
	./autogen.sh && \
	./configure && \
	make -j$(nproc) && \
	make install && \
	ldconfig && \
	rm -rf /tmp/libtpms

# Build swtpm
RUN git clone https://github.com/stefanberger/swtpm /tmp/swtpm && \
	cd /tmp/swtpm && \
	./autogen.sh && \
	./configure && \
	make -j$(nproc) && \
	make install && \
	rm -rf /tmp/swtpm

# Create our user/group
RUN echo uboot ALL=NOPASSWD: ALL > /etc/sudoers.d/zephyr
RUN useradd -m -U uboot
RUN mkdir -p $ZEPHYR_DIR && chown uboot $ZEPHYR_DIR
RUN mkdir -p /uboot && chown uboot /uboot

USER uboot:uboot

# None of the repos below are actually used. If we want to do this, we need
# to extract the right branch to the right path.

# # Set up the zephyr main repo so that gitlab won't have to fetch so much
# RUN mkdir $ZEPHYR_DIR/main && \
# 	git clone --depth=1 --branch chromeos-v2.4 --single-branch \
# 	https://chromium.googlesource.com/chromiumos/third_party/zephyr \
# 	$ZEPHYR_DIR/main/v2.4

# # Set up the zephyr cmsis repo too
# RUN mkdir $ZEPHYR_DIR/cmsis && \
# 	git clone --depth=1 --branch chromeos-v2.4 --single-branch \
# 	https://chromium.googlesource.com/chromiumos/third_party/zephyr/cmsis \
# 	$ZEPHYR_DIR/cmsis/v2.4

# Not used yet, and it is 400MB !!
# RUN mkdir $ZEPHYR_DIR/hal_stm32 && \
# 	git clone --depth=1 --branch chromeos-v2.4 --single-branch \
# 	https://chromium.googlesource.com/chromiumos/third_party/zephyr/hal_stm32 \
# 	$ZEPHYR_DIR/hal_stm32/v2.4

# Add Chromium OS test image, futility, etc.
ADD --chown=uboot:uboot large_files.tar.xz /uboot/

# Create the buildman config file
RUN /bin/echo -e "[toolchain]\nroot = /usr" > ~/.buildman
RUN /bin/echo -e "kernelorg = /opt/gcc-11.1.0-nolibc/*" >> ~/.buildman
RUN /bin/echo -e "arc = /opt/arc_gnu_2021.03_prebuilt_uclibc_le_archs_linux_install" >> ~/.buildman
RUN /bin/echo -e "\n[toolchain-prefix]\nxtensa = /opt/2020.07/xtensa-dc233c-elf/bin/xtensa-dc233c-elf-" >> ~/.buildman;
RUN /bin/echo -e "\nnds32 = /opt/nds32le-linux-glibc-v3-upstream/bin/nds32le-linux-" >> ~/.buildman;
RUN /bin/echo -e "\n[toolchain-alias]\nsh = sh2" >> ~/.buildman
RUN /bin/echo -e "\nriscv = riscv64" >> ~/.buildman
RUN /bin/echo -e "\nsandbox = x86_64" >> ~/.buildman
RUN /bin/echo -e "\nx86 = i386" >> ~/.buildman;
